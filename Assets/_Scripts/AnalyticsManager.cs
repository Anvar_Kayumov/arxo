﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsManager : MonoBehaviour
{
	public static AnalyticsManager instance;
	public bool enableAnalytics = false;

	private int _eventIndex = 0;

	private void Awake()
	{
		instance = this;
		DontDestroyOnLoad(this);
	}

	public void SendEvent(string eventName, Dictionary<string, object> eventParams = null)
	{
		if (!enableAnalytics)
			return;

		Dictionary<string, object> eventData = new Dictionary<string, object>();
		eventData["event"] = eventName;
		eventData["eventIndex"] = _eventIndex++;
		eventData["timestamp"] = UnixTimeNow();

		if (eventParams != null)
			eventData.Add("params", eventParams);

		//Debug.Log("Sending Event: " + eventName + " | " + YMMJSONUtils.JSONEncoder.Encode(eventData));
		AppMetrica.Instance.ReportEvent(eventName, eventParams);
	}

	private int UnixTimeNow()
	{
		var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
		return (int) timeSpan.TotalSeconds;
	}
}
