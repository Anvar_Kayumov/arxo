﻿using System.Linq;
using UnityEngine;

public class CellsController : MonoBehaviour
{
	public static CellsController instance { get; private set; }
	private DropCell[] _cells;

	private void Awake()
	{
		instance = this;
		_cells = FindObjectsOfType<DropCell>();
	}

	public DropCell GetRandomFreeCell()
	{
		DropCell[] freeCells = _cells.Where(x => x.isEmpty).ToArray();
		int randomNumber = Random.Range(0, freeCells.Length);
		return freeCells[randomNumber];
	}

	public void ResetCells()
	{
		for (int i = 0; i < _cells.Length; i++)
		{
			_cells[i].isEmpty = true;
		}
	}
}
