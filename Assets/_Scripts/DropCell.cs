﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropCell : MonoBehaviour, IPointerClickHandler
{
	public int rawIndex, columnIndex;
	[HideInInspector] public bool isEmpty = true;

	public void OnPointerClick(PointerEventData eventData)
	{
		if (isEmpty)
		{
			isEmpty = false;
			GameManager.instance.OnDropCellClicked(rawIndex, columnIndex, transform.position);
		}
	}
}
