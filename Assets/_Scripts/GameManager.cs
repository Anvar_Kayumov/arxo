﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance { get; private set; }
	public WinnerPopup winnerPopup;
	public GameOverPopup gameOverPopup;
	public Transform target;

	private char[,] _board;

	private int _currentRaw;
	private int _currentColumn;
	private int _totalSteps = 0;
	
	private char _currentObjectKey = 'X';

	private void Awake()
	{
		instance = this;
		_board = new char[3, 3];
		Application.targetFrameRate = 60;
	}

	public void OnDropCellClicked(int raw, int column, Vector3 dropCellPosition)
	{
		_totalSteps++;
		
		_currentRaw = raw;
		_currentColumn = column;
		_board[_currentRaw,_currentColumn] = _currentObjectKey;
		
		// Default value for initialization
		ObjectType objectType = ObjectType.X;
		
		switch (_currentObjectKey)
		{
			case 'X':
				objectType = ObjectType.X;
				break;

			case 'O':
				objectType = ObjectType.O;
				break;
		}

		GameObject dropObject = ObjectsPool.instance.Pop(objectType);
		dropObject.transform.position = dropCellPosition;
		dropObject.transform.SetParent(target);

		if (CheckWinner())
		{
			if (objectType == ObjectType.X)
				winnerPopup.ShowTheWinner(_currentObjectKey);
			else gameOverPopup.Show();

			return;
		}
		
		if (_totalSteps == 9)
		{
			StartNewGane();
			return;
		}

		SwitchNextPlayer();

		if (objectType == ObjectType.X)
			PutObjectRandom();
	}

	private void PutObjectRandom()
	{
		DropCell dropCell = CellsController.instance.GetRandomFreeCell();
		dropCell.isEmpty = false;
		OnDropCellClicked(dropCell.rawIndex, dropCell.columnIndex, dropCell.transform.position);
	}

	private void SwitchNextPlayer()
	{
		if (_currentObjectKey == 'X')
			_currentObjectKey = 'O';
		else _currentObjectKey = 'X';
	}

	private bool CheckWinner()
	{
		return CheckHorizontal() || CheckVertical() || CheckDiagonals();
	}

	private bool CheckVertical()
	{
		int counter = 0;
		for (int rawIndex = 0; rawIndex < 3; rawIndex++)
		{
			if (_board[rawIndex, _currentColumn] == _currentObjectKey)
				counter++;
		}

		if (counter == 3)
			return true;

		return false;
	}

	private bool CheckHorizontal()
	{
		int counter = 0;
		for (int columnIndex = 0; columnIndex < 3; columnIndex++)
		{
			if (_board[_currentRaw, columnIndex] == _currentObjectKey)
				counter++;
			else break;
		}
		
		if (counter == 3)
			return true;

		return false;
	}

	private bool CheckDiagonals()
	{
		int counter = 0;
		for (int index = 0; index < 3; index++)
		{
			if (_board[index, index] == _currentObjectKey)
				counter++;
			else break;
		}
		
		if (counter == 3)
			return true;

		counter = 0;

		int j = 2;
		for (int i = 0; i < 3; i++)
		{
			if (_board[i, j] == _currentObjectKey)
				counter++;
			else break;
			j--;
		}
		
		if (counter == 3)
			return true;

		return false;
	}

	private void ClearBoard()
	{
		_board = null;
		_board = new char[3, 3];
		ObjectsPool.instance.ResetPool();
	}

	public void StartNewGane()
	{
		ClearBoard();
		if (_currentObjectKey == 'X')
		{
			winnerPopup.Hide();
		}
		else if (_currentObjectKey == 'O')
		{
			gameOverPopup.Hide();
			_currentObjectKey = 'X';
		}
		
		_totalSteps = 0;
		
		CellsController.instance.ResetCells();
	}
}
