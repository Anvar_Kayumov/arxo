﻿using UnityEngine;

public class GameObjectTrackEventHandler : DefaultTrackableEventHandler {
	public GameObject[] targets;
	public GameObject aimImage;

	protected override void OnTrackingFound() {
		ToggleTarget(true);
	}

	protected override void OnTrackingLost() {
		ToggleTarget(false);
	}

	void ToggleTarget(bool r) {
		if (aimImage != null)
			aimImage.SetActive(!r);
		foreach (GameObject g in targets) {
			if (g != null)
				g.SetActive(r);
		}
	}
}
