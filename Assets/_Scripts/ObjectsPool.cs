﻿using UnityEngine;
using System.Collections.Generic;

public enum ObjectType
{
	X,
	O
}

public class ObjectsPool : MonoBehaviour
{
	public static ObjectsPool instance { get; private set; }

	public Stack<GameObject> xObjects = new Stack<GameObject>();
	public Stack<GameObject> oObjects = new Stack<GameObject>();

	private void Awake()
	{
		instance = this;
		InitPool();
	}

	private void InitPool()
	{
		InitPoolX();
		InitPoolO();
	}

	private void InitPoolX()
	{
		GameObject[] xGameObjects = GameObject.FindGameObjectsWithTag("X");
		int length = xGameObjects.Length;
		for (int i = 0; i < length; i++)
		{
			xGameObjects[i].transform.SetParent(transform);
			xGameObjects[i].transform.localPosition = Vector3.zero;
			xGameObjects[i].SetActive(false);
			xObjects.Push(xGameObjects[i]);
		}
	}

	private void InitPoolO()
	{
		GameObject[] oGameObjects = GameObject.FindGameObjectsWithTag("O");
		int length = oGameObjects.Length;
		for (int i = 0; i < length; i++)
		{
			oGameObjects[i].transform.SetParent(transform);
			oGameObjects[i].transform.localPosition = Vector3.zero;
			oGameObjects[i].SetActive(false);
			
			oObjects.Push(oGameObjects[i]);
		}
	}

	public GameObject Pop(ObjectType objectType)
	{
		switch (objectType)
		{
			case ObjectType.X:
				GameObject xObject = xObjects.Pop();
				xObject.transform.SetParent(null);
				xObject.SetActive(true);
				return xObject;
			case ObjectType.O:
				GameObject oObject = oObjects.Pop();
				oObject.transform.SetParent(null);
				oObject.SetActive(true);
				return oObject;
			default: return null;
		}
	}

	public void ResetPool()
	{
		InitPool();
	}
}
