﻿using UnityEngine;
using System.Collections.Generic;
using Vuforia;

public class SmoothCamera : MonoBehaviour {
	public Transform target;
	/*public int smoothingFrames = 10;

	private Quaternion smoothedRotation;
	private Vector3 smoothedPosition;

	private Queue<Quaternion> rotations;
	private Queue<Vector3> positions;

	Vector3 pos;
	Quaternion rot;

	public void OnTrackablesUpdated() {
		if (rotations.Count >= smoothingFrames) {
			rotations.Dequeue();
			positions.Dequeue();
		}

		rotations.Enqueue(rot);
		positions.Enqueue(pos);

		Vector4 avgr = Vector4.zero;
		foreach (Quaternion singleRotation in rotations) {
			Math3d.AverageQuaternion(ref avgr, singleRotation, rotations.Peek(), rotations.Count);
		}

		Vector3 avgp = Vector3.zero;
		foreach (Vector3 singlePosition in positions) {
			avgp += singlePosition;
		}
		avgp /= positions.Count;

		smoothedRotation = new Quaternion(avgr.x, avgr.y, avgr.z, avgr.w);
		smoothedPosition = avgp;
	}
*/
	void OnEnable() {
		/*if (positions != null) {
			positions.Clear();
			rotations.Clear();
			positions.Enqueue(transform.position);
			rotations.Enqueue(transform.rotation);
		}
		tr.position = pos = smoothedPosition = transform.position;
		tr.rotation = rot = smoothedRotation = transform.rotation;*/
		target.position = transform.position;
		target.rotation = transform.rotation;
	}
/*
	void Start() {
		rotations = new Queue<Quaternion>(smoothingFrames);
		positions = new Queue<Vector3>(smoothingFrames);
		VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
	}

	void Update() {
		pos = Vector3.Lerp(transform.position, pos, Mathf.Clamp01(0.05f * Time.deltaTime * 200));
		rot = Quaternion.Lerp(transform.rotation, rot, Mathf.Clamp01(0.05f * Time.deltaTime * 100));
	}
*/
	void Update() {
		target.position = Vector3.Lerp(transform.position, target.position, Mathf.Clamp01(0.05f * Time.deltaTime * 10));
		target.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Mathf.Clamp01(0.05f * Time.deltaTime * 5));
	}
}
