﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundsPlayer : MonoBehaviour
{
	private AudioSource _audioSource;
	
	public static SoundsPlayer instance;

	private void Awake()
	{
		instance = this;
		_audioSource = GetComponent<AudioSource>();
	}

	public void PlaySoundOneShot(AudioClip clip)
	{
		_audioSource.PlayOneShot(clip);
	}
}
