﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class BasePopup : MonoBehaviour
{
	public CanvasGroup fadeScreen;
	public RectTransform content;
	public AudioClip popupSound;
	public System.Action onClose;

	readonly Vector3 hidePosition = new Vector3(0, -2048);
	readonly Vector3 showPosition = Vector3.zero;

	EventSystem eventSystem;

	public void Show()
	{
		Show(null);
	}

	public void Hide()
	{
		Hide(() => { });
	}

	public virtual void Show(System.Action onComplete = null)
	{
		// Disable touch events
		if (eventSystem == null)
			eventSystem = EventSystem.current;
		if (eventSystem != null)
			eventSystem.enabled = false;

		if (!gameObject.activeSelf)
		{
			gameObject.SetActive(true);
			content.anchoredPosition = hidePosition;
			fadeScreen.alpha = 0;
		}

		transform.SetAsLastSibling();

		SoundsPlayer.instance.PlaySoundOneShot(popupSound);

		Tween.Stop(content, "popup");
		Tween.Value(0.3f).From(fadeScreen.alpha).To(1).OnUpdate((f) => fadeScreen.alpha = f).Target(content).Tags("popup")
			.Start();
		Tween.Move(content, 0.3f, 0.1f).To(showPosition).OnComplete(() =>
		{
			// Enable touch events
			if (eventSystem != null)
				eventSystem.enabled = true;

			if (onComplete != null)
				onComplete();
		}).Tags("popup").Start();

	}

	public virtual void Hide(System.Action onComplete)
	{
		if (!gameObject.activeSelf)
			return;
		
		// Disable touch events
		if (eventSystem == null)
			eventSystem = EventSystem.current;
		if (eventSystem != null)
			eventSystem.enabled = false;

		Tween.Stop(content, "popup");
		Tween.Value(0.3f, 0.1f).From(fadeScreen.alpha).To(0).OnUpdate((f) => fadeScreen.alpha = f).OnComplete(() =>
		{
			gameObject.SetActive(false);

			// Enable touch events
			if (eventSystem != null)
			{
				eventSystem.enabled = true;
				eventSystem = null;
			}

			if (onComplete != null)
				onComplete();

			if (onClose != null)
				onClose();
			onClose = null;
		}).Target(content).Tags("popup").Start();
		Tween.Move(content, 0.3f).To(hidePosition).Tags("popup").Start();
	}
	
	public void SendEvent(string eventName, Dictionary<string, object> eventParams = null)
	{
		AnalyticsManager.instance.SendEvent(eventName, eventParams);
	}
}