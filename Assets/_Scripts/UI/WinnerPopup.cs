﻿using UnityEngine.UI;

public class WinnerPopup : BasePopup
{
	public Text winnerObjectText;

	public void ShowTheWinner(char winnerObjectKey)
	{
		winnerObjectText.text = winnerObjectKey.ToString();
		base.Show();
	}
}
